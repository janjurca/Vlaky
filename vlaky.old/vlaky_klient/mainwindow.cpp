#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsView>
#include <QTimer>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>
#include <QMainWindow>
#include <QVector>
#include <QLabel>
#include <QtGui>
#include <QtCore>
#include <QGridLayout>
#include <QtWidgets>

#define port 4545
#define vyska 200
#define sirka 200

QList< QLabel* > vlaky;
QList< int > stop;
int mnozstvi_vlaku = 0;

int cislo_klienta = 0;
int i = 0;

QTimer *timer = new QTimer;
QStringList datagram_list;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    udpSocket = new QUdpSocket;
    udpSocket->bind(port, QUdpSocket::ShareAddress);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(processPendingDatagrams()));
    connect(timer, SIGNAL(timeout()), this, SLOT(posun()));
    timer->start(20);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::posun(){
    for(int i = 0;i < vlaky.size(); i++){
        vlaky[i]->setGeometry(vlaky[i]->x() + 1, vlaky[i]->y(),sirka,vyska);

        if((vlaky[i]->x() + vlaky[i]->width()) > window()->width() && stop[i] == 0){
            QByteArray datagram = "s " + QByteArray::number(cislo_klienta) + " "+ QByteArray::number(vlaky[i]->y());
            qWarning() << "bylo poslano" << datagram;
            udpSocket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, port);
            stop[i] = 1;
        }
        if( vlaky[i]->x()  > window()->width()){
            vlaky.removeAt(i);
            stop.removeAt(i);
            mnozstvi_vlaku--;
        }
    }

}

void MainWindow::processPendingDatagrams()
{
    QPixmap vlak_image("vlacek.png");
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(datagram.data(), datagram.size());
        qWarning() << datagram;
        QString datagram_qstr(datagram);
        datagram_list = datagram_qstr.split(" ");

    }
    if(datagram_list[0] == "k"){
        if(cislo_klienta != 0 && cislo_klienta == datagram_list[1].toInt()){
            vlaky << new QLabel( this );
            stop << 0;
            vlaky[mnozstvi_vlaku]->setPixmap(vlak_image);
            qWarning() << "test 1";
            vlaky[mnozstvi_vlaku]->setGeometry(0 - sirka,datagram_list[2].toInt(),sirka,vyska);
            layout()->addWidget(vlaky[mnozstvi_vlaku]);
            mnozstvi_vlaku++;
        }
        else{
            qWarning() << "neni to pro mě";
        }
    }

}

void MainWindow::on_pushButton_clicked()
{
    cislo_klienta = ui->cislo_klienta->text().toInt();
    ui->cislo_klienta->setVisible(false);
    ui->pushButton->setVisible(false);
    QByteArray datagram = "sz " + QByteArray::number(cislo_klienta); // odeslani paketu o tom ze ziju
    udpSocket->writeDatagram(datagram.data(), datagram.size(), QHostAddress::Broadcast, port);

}

void MainWindow::mouseReleaseEvent ( QMouseEvent * event )
{
    QPixmap vlak_image("vlacek.png");
    vlak_image.scaledToWidth(sirka);
    vlak_image.scaledToHeight(vyska);
    if(cislo_klienta != 0){
  if(event->button() == Qt::RightButton)
  {
     qWarning() << event->x();
     qWarning() << event->y();

     vlaky << new QLabel( this );
     stop << 0;
     vlaky[mnozstvi_vlaku]->setPixmap(vlak_image);
     qWarning() << "test 1";
     vlaky[mnozstvi_vlaku]->setGeometry(event->x(),event->y(),sirka,vyska);
     layout()->addWidget(vlaky[mnozstvi_vlaku]);
     mnozstvi_vlaku++;

   }
}
}
