#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtNetwork>
#include <QMouseEvent>
#include <QUrl>
#include <QNetworkReply>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void startRequest(const QUrl &requestedUrl);


private slots:
    void posun();

    void getFile();
    void downloadFile(QString url);
    void httpFinished();
    void httpReadyRead();

    void send_get_request(QString qurl);
    void httpFinished_get();

    void parseString(QString qstr);

    void mouseReleaseEvent ( QMouseEvent * event );
    void on_pushButton_clicked();
    void add_train(int x, int y);
    void remove_train(int idx);


    void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;

    QString file_content;
    QUrl url;
    QNetworkAccessManager qnam;
    QNetworkReply *reply;

    QUrl url_get;

    QList< QNetworkAccessManager* > qnetm_list;

    bool httpRequestAborted;
    bool http_proccesing = false;
};

#endif // MAINWINDOW_H
